package com.example.sample_intent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by g on 10/3/2016.
 */

public class SubActivity1 extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subactivity1);
    }
}
