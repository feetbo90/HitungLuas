package com.dhuocreative.g.firebasetest.scheduller;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.dhuocreative.g.firebasetest.App;
import com.dhuocreative.g.firebasetest.web.ApiService;
import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.squareup.otto.Subscribe;

import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by g on 11/14/2016.
 */

public class DemoSyncJob extends Job {
    private ApiService api;
    protected App application;
    public static final String TAG = "job_demo_tag";
    public static Result resultcoy;

    private static int jobId;
    public static void scheduleJob() {
        new JobRequest.Builder(DemoSyncJob.TAG)
                .setExecutionWindow(30_000L, 40_000L)
                .build()
                .schedule();
    }

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        // run your job here
        api = application.getApi();
        api.getLogin("6",  callbackPost);
        Log.e("masuk" , "masuk Result Run Job");
        if(resultcoy == Result.SUCCESS)
        {
            return Result.SUCCESS;
        }else return Result.FAILURE;
    }

    private Callback<String> callbackPost = new Callback<String>() {
        @Override
        public void success(String result, Response response) {
            application.getEventBus().post(result);
        }

        @Override
        public void failure(RetrofitError error) {
             application.getEventBus().post(error);
        }
    };

    public void schedulePeriodicJob(){
        Log.e("masuk schedule", "masuk");
        jobId = new JobRequest.Builder(DemoSyncJob.TAG)
                .setPeriodic(TimeUnit.MINUTES.toMillis(15), TimeUnit.MINUTES.toMillis(5))
                .setPersisted(true)
                .build()
                .schedule();
    }

    private void cancelJob() {
        JobManager.instance().cancel(jobId);
    }

    @Subscribe
    public void getListSuccess(String result) {



           Log.d("masuk request", result);
            resultcoy = Result.SUCCESS;
    }

    @Subscribe
    public void getListFailed(RetrofitError error) {

        Log.d("gagal masuk ", error.getResponse().toString());


    }
}