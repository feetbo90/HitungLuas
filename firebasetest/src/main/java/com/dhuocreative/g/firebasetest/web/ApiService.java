package com.dhuocreative.g.firebasetest.web;





import com.dhuocreative.g.firebasetest.model.Category;
import com.dhuocreative.g.firebasetest.model.Post;
import com.dhuocreative.g.firebasetest.model.PostDetail;
import com.dhuocreative.g.firebasetest.model.Tag;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

public interface ApiService {
    @GET("/api/posts/latest/post/{page}")
    void getLatestPost(@Path("page") int page, Callback<Post> callback);

    @GET("/android/kardopadua/showviewsnew.php")
    void getShowViews(@Query("page") int page, Callback<Post> callback);

    @GET("/android/ceknotifikasi.php")
    void getLogin(@Query("jumlah") String jumlah, Callback<String> callback);

    @GET("/api/posts/latest/{type}/{page}")
    void getPostByType(@Path("type") String type, @Path("page") int page, Callback<Post> callback);

    @GET("/api/posts/by_category/{kategori}/{page}")
    void getPostByCategory(@Path("kategori") String kategori, @Path("page") int page, Callback<Post> callback);

    @GET("/api/posts/by_tag/{tag}/{page}")
    void getPostByTag(@Path("tag") String tag, @Path("page") int page, Callback<Post> callback);

    @FormUrlEncoded
    @POST("/api/posts/search")
    void searchPost(@Field("keyword") String keyword, @Field("page") int page, Callback<Post> callback);

    @GET("/api/posts/detail/{post_id} ")
    void getPostDetail(@Path("post_id") int postId, Callback<PostDetail> callback);

    @GET("/api/category/available/{page}")
    void getAvailableCategory(@Path("page") int page, Callback<Category> callback);

    @GET("/api/tags/popular/{page}")
    void getPopularTag(@Path("page") int page, Callback<Tag> callback);
}
