package com.dhuocreative.g.firebasetest.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class Post {
    //public int code;
    public List<Result> result;

    @Parcel
    public static class Result {
        public int id_berita;
        @SerializedName("foto") public String foto;
        public String judulberita;
        public String deskripsi;
        //@SerializedName("thumbnail_small") public String thumbnailSmall;

    }

}
