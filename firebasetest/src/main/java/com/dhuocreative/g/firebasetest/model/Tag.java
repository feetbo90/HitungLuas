package com.dhuocreative.g.firebasetest.model;

import org.parceler.Parcel;

import java.util.List;

public class Tag {
    public int code;
    public List<Result> result;

    @Parcel
    public static class Result {
        public String name;
        public String slug;
        public int count;
    }
}
