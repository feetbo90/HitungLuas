package com.dhuocreative.g.firebasetest;

import android.app.Application;

import com.dhuocreative.g.firebasetest.scheduller.DemoJobCreator;
import com.dhuocreative.g.firebasetest.web.ApiService;
import com.dhuocreative.g.firebasetest.web.RestClient;
import com.evernote.android.job.JobManager;
import com.squareup.otto.Bus;

/**
 * Created by g on 11/14/2016.
 */

public class App extends Application {
    private Bus eventBus;
    private ApiService apiService;
    @Override
    public void onCreate() {
        super.onCreate();
        JobManager.create(this).addJobCreator(new DemoJobCreator());
        getEventBus().register(this);
    }

    public ApiService getApi() {
        if (apiService == null) apiService = RestClient.getService();
        return apiService;
    }
    public Bus getEventBus() {
        if (eventBus == null) eventBus = new Bus();
        return eventBus;
    }

}