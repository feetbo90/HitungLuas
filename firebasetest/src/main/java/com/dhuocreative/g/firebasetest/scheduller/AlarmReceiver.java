package com.dhuocreative.g.firebasetest.scheduller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.dhuocreative.g.firebasetest.App;
import com.dhuocreative.g.firebasetest.activity.MainActivity;
import com.dhuocreative.g.firebasetest.web.ApiService;
import com.evernote.android.job.Job;
import com.squareup.otto.Subscribe;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by g on 11/14/2016.
 */

public class AlarmReceiver extends BroadcastReceiver {
    private ApiService api;
    protected App application;


    @Override
    public void onReceive(Context arg0, Intent arg1) {
        // For our recurring task, we'll just display a message
        application = (App)arg0.getApplicationContext();

        application.getEventBus().unregister(arg0);
        application.getEventBus().register(arg0);
        api = application.getApi();
        api.getLogin("6",  callbackPost);
        Toast.makeText(arg0, "I'm running", Toast.LENGTH_SHORT).show();


    }
    private Callback<String> callbackPost = new Callback<String>() {
        @Override
        public void success(String result, Response response) {
            application.getEventBus().post(result);
        }

        @Override
        public void failure(RetrofitError error) {
            application.getEventBus().post(error);
        }
    };

    @Subscribe
    public void getListSuccess(String result) {



        Log.d("masuk request", result);
    }

    @Subscribe
    public void getListFailed(RetrofitError error) {

        Log.d("gagal masuk ", error.getResponse().toString());


    }
}